extends KinematicBody2D

var speed = 500
var dodgeSpeed = 1000
var velocity = Vector2()
var lastDirection = Vector2.DOWN
var state = MOVE
var dodgeFrame = 0
var attack = load("res://Scenes/Attack.tscn")

enum {
	MOVE,
	DODGE
	REST,
	ATTACK
}

func get_input():
	# Detect up/down/left/right keystate and only move when pressed.
	velocity = Vector2()
	if Input.is_action_pressed('ui_right'):
		velocity.x += 1
	if Input.is_action_pressed('ui_left'):
		velocity.x -= 1
	if Input.is_action_pressed('ui_down'):
		velocity.y += 1
	if Input.is_action_pressed('ui_up'):
		velocity.y -= 1
	if Input.is_action_just_pressed("ui_dodge"):
		var newAttack = attack.instance()
		newAttack.position = $AttackPos.position
		newAttack.transform.rotated(lastDirection.angle())
		add_child(newAttack)
	if Input.is_action_just_pressed("tp"):
		if get_node("/root/PhysicsSword") != null:
			if(get_node("SwordRing").throw == true):
				var newpos = get_node("/root/PhysicsSword").position
				get_node("/root/PhysicsSword").queue_free()
				global_position = newpos + global_position - $CollisionShape2D.global_position
				get_node("SwordRing").throw = false
	if Input.is_action_just_pressed("recall"):
		if get_node("/root/PhysicsSword") != null:
			if(get_node("SwordRing").throw == true):
				get_node("/root/PhysicsSword").queue_free()
				get_node("SwordRing").throw = false
	velocity = velocity.normalized() * speed

func animation():
	if(state == MOVE):
		if velocity.x > 0:
			$Sprites/Body.play("run_right")
			lastDirection = Vector2.RIGHT
		if velocity.x < 0:
			$Sprites/Body.play("run_left")
			lastDirection = Vector2.LEFT
		if velocity.y > 0 and velocity.x == 0:
			$Sprites/Body.play("run_down")
			lastDirection = Vector2.DOWN
		if velocity.y < 0 and velocity.x == 0:
			$Sprites/Body.play("run_up")
			lastDirection = Vector2.UP

		if velocity.x == 0 and velocity.y == 0:
			if lastDirection == Vector2.RIGHT:
				$Sprites/Body.play("idle_right")
			if lastDirection == Vector2.LEFT:
				$Sprites/Body.play("idle_left")
			if lastDirection == Vector2.DOWN:
				$Sprites/Body.play("idle_down")
			if lastDirection == Vector2.UP:
				$Sprites/Body.play("idle_up")	

	if state == DODGE:
		if velocity.x > 0:
			$Sprites/Body.play("run_right")
		if velocity.x < 0:
			$Sprites/Body.play("run_left")
		if velocity.y > 0 and velocity.x == 0:
			$Sprites/Body.play("dodge_down")
		if velocity.y < 0 and velocity.x == 0:
			$Sprites/Body.play("run_up")
	
		if dodgeFrame > 0:
			velocity = velocity.normalized() * dodgeSpeed
			dodgeFrame -= 1
		else:
			dodgeFrame = 20
			state = MOVE

func _ready():
	pass
	
func _process(delta):
	animation()

func _physics_process(delta):
	if state == MOVE:
		get_input()
	move_and_collide(velocity * delta)

