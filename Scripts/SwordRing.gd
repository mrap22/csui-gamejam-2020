extends Node2D

var animInit = false
var animOut = false
var animHold = false
var physicsSword = load("res://Scenes/PhysicsSword.tscn")
var swordInstance
var throw = false

func get_input():
	if Input.is_action_pressed("ui_sword_throw"):
		animInit = true
	if Input.is_action_just_released("ui_sword_throw"):
		animOut = true

func animation():
	if throw:
		$Ring/Sword/SwordSprite.visible = false
	else:
		$Ring/Sword/SwordSprite.visible = true
	
	if animInit == true and animHold == false and animOut == false:
		visible = true
		$Ring/Sword/SwordSprite.play("summon")

	if $Ring/Sword/SwordSprite.animation == "summon" && \
	$Ring/Sword/SwordSprite.frame == $Ring/Sword/SwordSprite.frames.get_frame_count("summon")-1:
		$Ring/Sword/SwordSprite.play("default")
		animHold = true

	if animOut:
		animHold = false
		$Ring/Sword/SwordSprite.play("desummon")

	if $Ring/Sword/SwordSprite.animation == "desummon" && \
	$Ring/Sword/SwordSprite.frame == $Ring/Sword/SwordSprite.frames.get_frame_count("desummon")-1:
		visible = false
		animOut = false
		animInit = false

	if animHold:
		$Ring/Sword/SwordSprite.play("default")


func _ready():
	#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	swordInstance = physicsSword.instance()
	
func _input(event):
   # Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		print("Mouse Click/Unclick at: ", event.position)
	elif event is InputEventMouseMotion:
		$Ring/Sword.rotate(event.relative.x * 0.01)
		print("Mouse Motion at: ", event.relative.x, event.relative.y)

func _process(delta):
	get_input()
	animation()

func _physics_process(delta):
	if Input.is_action_pressed("ui_sword_throw_action") and throw == false and is_visible():
		swordInstance = physicsSword.instance()
		throw = true
		swordInstance.transform = $Ring/Sword/SwordSprite.global_transform
		swordInstance.rotation = $Ring/Sword.rotation
		get_node("/root").add_child(swordInstance)
		swordInstance.apply_central_impulse(Vector2(-sin($Ring/Sword.rotation), cos($Ring/Sword.rotation)) * 1000)
		
